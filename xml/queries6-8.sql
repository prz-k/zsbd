--
-- 6: tickets purchase
--

--drop table ParticipantsXmlBak;
--create table ParticipantsXmlBak as select * from ParticipantsXml;

-- helper function
create or replace function fakeParticipant
  return XmlType 
is
  theResult XmlType;
begin
  select XmlElement(name "participant",
    XmlAttributes(round(dbms_random.value * 20000) as "userId"),
    XmlElement(name "soldticket",
      XmlAttributes(
        round(dbms_random.value * 30000) as "ticketId",
        current_timestamp as "purchaseDate",
        round(dbms_random.value * 100, 2) as "price")),
    XmlElement(name "fake"))
  into theResult from dual;
  return theResult;
end;

-- Note: This example is about demonstrating XML operation, so I _don't_
-- care about foreign key integrity and just put *some* numbers as ids.
-- Besides, transaction rollback is our friend anyway :)
update ParticipantsXmlBak
set participants = 
  appendChildXml(participants, '//participants', fakeParticipant())
where 1024 <= eventId and eventId < 2048; 

-- 1024 rows
select * from ParticipantsXmlBak where 1024 <= eventId and eventId < 2048;

-- sql equivalent
declare
  pcId number;
begin
  for evtId in 1024..2048 loop
    pcId := 1000000 + evtId;
    insert into participants (id, eventId, userId)
      values (pcId, evtId, round(dbms_random.value * 20000));
    insert into soldtickets (id, ticketId, participantId, purchaseDate, price)
      values (pcId, round(dbms_random.value * 30000), pcId, 
              current_timestamp, round(dbms_random.value * 100, 2));
  end loop;
end;
/

--
-- 7: remove participants and soldtickets for some events
--

--drop table ParticipantsXmlBak;
--create table ParticipantsXmlBak as select * from ParticipantsXml;

select p.participants.getClobVal()
from ParticipantsXmlBak p
where existsNode(p.participants, '//participants[@eventId > 19000]') = 1;

select p.participants.getClobVal()
from ParticipantsXmlBak p
where p.eventid > 18000 and p.eventid <= 19000;

-- Note that the ''[@eventId > 19000]'' (*) condition is duplicated. We could
-- do without the where clause here, as the XPath in deleteXml is going to
-- match only the nodes that satisfy the condition (*) _while deleting nodes_, 
-- but still _all_ rows of the table will be affected by the update statement 
-- (with unmodified xml though). The where clause however reduces the number of
-- rows to be updated, and thus speed up the query. But I'm going to leave it as
-- it is anyway.
update ParticipantsXmlBak p
set p.participants = deleteXml(p.participants, 
                               '//participants[@eventId > 19000]/participant')
where existsNode(p.participants, '//participants[@eventId > 19000]') = 1;

-- sql equivalent
begin
  delete from Soldtickets s 
    where s.ticketId in (select t.id from Tickets t where t.eventId > 19000);
  delete from Participants p
    where p.eventId > 19000;
end;
/

--
-- 8: news (message only) from events that have a coordinator 
--    who has coordinated more than one event
-- 

-- with binary XMLType    
with filtered_events as (  
  select evts.eventId
  from Users u,
    XmlTable('/userInfo/coordinatedEvents/event' 
             passing u.userInfo 
             columns eventId number path 'id') evts    
  where existsNode(u.userInfo, 
                   '/userInfo/coordinatedEvents[count(event) > 1]') = 1
)
select e.id, msgs.*
from Events e,
  XmlTable('/news/newsEntry' passing e.news
           columns message varchar(2048) path 'message') msgs
where extractValue(e.news, '/news/@eventId') in (select eventId from filtered_events);

-- with CLOB XMLType (much slower!)
with filtered_events as (  
  select evts.eventId
  from Users u,
    XmlTable('/userInfo/coordinatedEvents/event' 
             passing u.userInfo 
             columns eventId number path 'id') evts    
  where existsNode(u.userInfo, 
                   '/userInfo/coordinatedEvents[count(event) > 1]') = 1
)
select e.id, msgs.*
from Events e,
  XmlTable('/news/newsEntry' passing e.news_cl
           columns message varchar(2048) path 'message') msgs
where extractValue(e.news_cl, '/news/@eventId') in (select eventId from filtered_events);

-- sql equivalent
-- 15983 events, 23956 news entries, 5008 coordinators
select e.id, n.*
from Events e 
  join News n on n.eventId = e.id
where e.id in
  (select c1.eventId
   from Coordinators c1, Coordinators c2
   where c1.userId = c2.userId
     and c1.eventId <> c2.eventId);
                               