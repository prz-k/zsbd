--------------------------------------------------------------------------------
-- Users.userInfo
--------------------------------------------------------------------------------

-- sample select
select u.id,
  XmlElement(
    name "userInfo",
    XmlElement(name "id", u.id),
    XmlElement(name "name", u.name),
    XmlElement(name "surname", u.surname),
    XmlElement(name "email", u.email),
    XmlElement(name "city", XmlAttributes(r.country as "country"), r.city),
    XmlElement(name "memberships",
      (select XmlAgg(
         XmlElement(name "group",
           XmlAttributes(m1.groupId as "id", g1.name as "name")
       ))
       from Memberships m1 join Groups g1 on g1.id = m1.groupId
       where m1.userid = u.id)
    ),
    XmlElement(name "coordinatedEvents",
      (select XmlAgg(
         XmlElement(name "event",
           XmlElement(name "id", e2.id),
           XmlElement(name "title", e2.title),
           XmlElement(name "date", e2.eventDateTime),
           XmlElement(name "role", c2.role)
         )
       )
       from Coordinators c2 join Events e2 on c2.eventId = e2.id 
       where c2.userId = u.id)
    )
  ) as xmlres
from Users u
  join Regions r on u.defaultRegion = r.id
where u.id = 28;  -- ARBITRARY CONST!

-- 
alter table Users add (userInfo XmlType)
XmlType column userInfo store as basicfile binary xml;

-- write xml (update) to Users.userInfo
update Users u_users set (userInfo) = (
  select
    XmlElement(
      name "userInfo",
      XmlElement(name "id", u.id),
      XmlElement(name "name", u.name),
      XmlElement(name "surname", u.surname),
      XmlElement(name "email", u.email),
      XmlElement(name "city", XmlAttributes(r.country as "country"), r.city),
      XmlElement(name "memberships",
        (select XmlAgg(
           XmlElement(name "group",
             XmlAttributes(m1.groupId as "id", g1.name as "name")
         ))
         from Memberships m1 join Groups g1 on g1.id = m1.groupId
         where m1.userid = u.id)
      ),
      XmlElement(name "coordinatedEvents",
        (select XmlAgg(
           XmlElement(name "event",
             XmlElement(name "id", e2.id),
             XmlElement(name "title", e2.title),
             XmlElement(name "date", e2.eventDateTime),
             XmlElement(name "role", c2.role)
           )
         )
         from Coordinators c2 join Events e2 on c2.eventId = e2.id 
         where c2.userId = u.id)
      )
    ) as xmlres
  from Users u
    join Regions r on u.defaultRegion = r.id
  where u.id = u_users.id
  )
;

--------------------------------------------------------------------------------
-- Events.coordinators
--------------------------------------------------------------------------------

-- sample select
select c.eventId,
  XmlElement(
    name "coordinators",
    XmlAgg(
      XmlElement(
        name "coordinator", 
        XmlAttributes(c.userId as "userId"),
        XmlElement(name "name", u.name || ' ' || u.surname),
        XmlElement(name "role", c.role)
      )
    )
  ) as xmlres
from Coordinators c
  join Users u on u.id = c.userId
group by c.eventId;

--
alter table Events add (coordinators_vc varchar2(4000));
alter table Events add (coordinators XmlType);

-- write xml (update) to Events.coordinators_vc as VARCHAR2(4000)
-- these are usually short documents (couple of hundred characters tops,
-- i guess) and should fit
update Events e set (coordinators_vc) = (
  select XmlElement(name "coordinators",
    XmlAgg(
      XmlElement(
        name "coordinator", 
        XmlAttributes(c.userId as "userId"),
        XmlElement(name "name", u.name || ' ' || u.surname),
        XmlElement(name "role", c.role)
      )
    )
  ).getStringVal()
  from Coordinators c
    join Users u on u.id = c.userId
  where c.eventId = e.id
  group by c.eventId
)
;

-- write xml (update) to Events.coordinators as XmlType
update Events e set (coordinators) = (
  select XmlElement(name "coordinators",
    XmlAgg(
      XmlElement(
        name "coordinator", 
        XmlAttributes(c.userId as "userId"),
        XmlElement(name "name", u.name || ' ' || u.surname),
        XmlElement(name "role", c.role)
      )
    )
  )
  from Coordinators c
    join Users u on u.id = c.userId
  where c.eventId = e.id
  group by c.eventId
)
;

--------------------------------------------------------------------------------
-- Events.news
--------------------------------------------------------------------------------

-- sample select
select n.eventId,
  XmlElement(name "news", 
    XmlAttributes(n.eventId as "eventId"),
    XmlAgg(
      XmlElement(name "newsEntry",
        XmlElement(name "author", u.name || ' ' || u.surname),
        XmlElement(name "authorId", n.authorId),
        XmlElement(name "date", n.creationdate),
        XmlElement(name "message", XmlCData(n.message))
      )
    )
  ) as xmlres
from News n
  join Users u on u.id = n.authorid
group by n.eventid;

--
alter table Events add (news_cl XmlType)
XmlType column news_cl store as clob;

alter table Events add (news XmlType);

-- write xml (update) to Events.news_cl as XmlType(clob)
update Events e set (news_cl) = (
  select XmlElement(name "news", 
      XmlAttributes(n.eventId as "eventId"),
      XmlAgg(
        XmlElement(name "newsEntry",
          XmlElement(name "author", u.name || ' ' || u.surname),
          XmlElement(name "authorId", n.authorId),
          XmlElement(name "date", n.creationdate),
          XmlElement(name "message", XmlCData(n.message))
        )
      )
    ) as xmlres
  from News n
    join Users u on u.id = n.authorid
  where n.eventid = e.id
  group by n.eventid
)
;

-- write xml (update) to Events.news as XmlType(default: binary)
update Events e set (news) = (
  select XmlElement(name "news", 
      XmlAttributes(n.eventId as "eventId"),
      XmlAgg(
        XmlElement(name "newsEntry",
          XmlElement(name "author", u.name || ' ' || u.surname),
          XmlElement(name "authorId", n.authorId),
          XmlElement(name "date", n.creationdate),
          XmlElement(name "message", XmlCData(n.message))
        )
      )
    ) as xmlres
  from News n
    join Users u on u.id = n.authorid
  where n.eventid = e.id
  group by n.eventid
)
;

--------------------------------------------------------------------------------
-- ParticipantsXml
--------------------------------------------------------------------------------

-- sample select
select p.eventId, 
  XmlElement(name "participants",
    XmlAttributes(p.eventId as "eventId"),
    XmlAgg(
      XmlElement(name "participant",
        XmlAttributes(p.userId as "userId"),
        (select XmlAgg(
          XmlElement(name "soldticket", 
            XmlAttributes(
              s.ticketId as "ticketId",
              s.purchaseDate as "purchaseDate",
              s.price as "price"
            )
          )) 
         from Soldtickets s
           join Participants p1 on p1.id = s.participantId
         where p1.eventId = p.eventId and p1.userId = p.userId)
      )
    )
  ) as xmlres
from Participants p
where p.eventId = 1024  -- ARBITRARY CONSTANT! 
group by p.eventId;

-- create table
drop table ParticipantsXml;
create table ParticipantsXml (eventId number primary key, participants XmlType);

-- get eventIds
delete from ParticipantsXml;
insert into ParticipantsXml (eventId) select distinct eventId from Participants;

-- WARNING WARNING WARNING: TAKES __EXTREMELY__ LONG...
-- write xml (update) as XmlType(default: binary)
update ParticipantsXml px set (participants) = (
  select 
    XmlElement(name "participants",
      XmlAttributes(p.eventId as "eventId"),
      XmlAgg(
        XmlElement(name "participant",
          XmlAttributes(p.userId as "userId"),
          (select XmlAgg(
            XmlElement(name "soldticket", 
              XmlAttributes(
                s.ticketId as "ticketId",
                s.purchaseDate as "purchaseDate",
                s.price as "price"
              )
            )) 
           from Soldtickets s
             join Participants p1 on p1.id = s.participantId
           where p1.eventId = p.eventId and p1.userId = p.userId)
        )
      )
    ) as xmlres
  from Participants p
  where p.eventId = px.eventId
  group by p.eventId
)
;



