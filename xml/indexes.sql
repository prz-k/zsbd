-- Note:
-- only XDB.XmlIndex type indexes are being created. Function-based indexes
-- probably won't work out for us - we'd need to use a function like
-- extractValue which works only if there's only one element macthing the given
-- XPath, and in our case we expect there are more.

create index pxml_participants_ix on ParticipantsXml(Participants)
indextype is XDB.XmlIndex
parameters ('paths (include (//participant/@userId))');

create index evt_news_ix on Events(news)
indextype is XDB.XmlIndex
parameters ('paths (include (//newsEntry/authorId))');

create index evt_news_cl_ix on Events(news_cl)
indextype is XDB.XmlIndex
parameters ('paths (include (//newsEntry/authorId))');

create index evt_coords_ix on Events(coordinators)
indextype is XDB.XmlIndex
parameters ('paths (include (//coordinator/@userId))');

---
---
---

-- Here is a dummy function-based index though:
create index user_userinfo_city_ix 
on Users(extractValue(userInfo, '/userInfo/city'));

-- versus general XmlIndex (no paths)
create index user_userinfo_xml_ix on Users(userInfo)
indextype is XDB.XmlIndex;

-- sample queries

-- no index: 1.374s
-- XDB.XmlIndex: 0.653s
-- should not use the function-based index
select extractValue(u.userInfo, '/userInfo/id') as id,
       extractValue(u.userInfo, '/userInfo/name') as name,
       extractValue(u.userInfo, '/userInfo/surname') as surname,
       extractValue(u.userInfo, '/userInfo/email') as email
from Users u
where existsNode(u.userInfo, '/userInfo[city = "Naples"]') = 1;

-- no index: 0.427s
-- XDB.XmlIndex: 0.303s
-- somehow it does not want to use the function-based index :(
select extractValue(u.userInfo, '/userInfo/id') as id,
       extractValue(u.userInfo, '/userInfo/name') as name,
       extractValue(u.userInfo, '/userInfo/surname') as surname,
       extractValue(u.userInfo, '/userInfo/email') as email
from Users u
where extractValue(userInfo, '/userInfo/city') = 'Naples';

-- no index: 0.67s
-- but when i don't access the xml field, it does use
-- the function based index...: 0.2s
-- XDB: 0.2s
select *
from Users u
where extractValue(userInfo, '/userInfo/city') = 'Naples';
