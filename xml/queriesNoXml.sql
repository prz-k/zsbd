--- 1
select users.* from users where 
(select count(*) from memberships where userid = users.id group by userid)>1;

--- 2
select users.name, users.surname, sum(soldtickets.price) from users 
left join participants on users.ID = participants.userid 
left join soldtickets on soldtickets.participantid = participants.id 
group by soldtickets.participantid, users.surname, users.name;

--- 3
select news.* from news left join coordinators on news.AUTHORID = coordinators.userid 
where news.eventid = coordinators.eventid;

--- 4
