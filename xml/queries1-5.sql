-- 1: users in more than one group

select users.id, users.name, users.surname from users where 
(select count(*) from memberships where userid = users.id group by userid) >= 2;

-- seems faster:
select users.id, users.name, users.surname
from users, memberships
where users.id = memberships.userid
group by users.id, users.name, users.surname
having count(*) >= 2;

SELECT * FROM XMLTABLE(
  'for $u in fn:collection("oradb:/ZSBD_W1/USERS/ROW/USERINFO")/userInfo
   where count($u/memberships/group) >= 2 
   return <user>{$u/*}</user>'
);

-- sql/xml may be a little simpler here  
select u.userInfo
from Users u
where existsNode(u.userInfo, '/userInfo/memberships[count(group) >= 2]') = 1;


-- 2 total price of all tickets bought by a user

select sum(soldtickets.price) as total from participants, soldtickets
where participants.userid = 18102 and soldtickets.participantid = participants.id;

select sum(to_number(extractValue(p.participants, '//participant[@userId=18102]/soldticket/@price'))) as total
from ParticipantsXml p
where existsNode(p.participants, '//participant[@userId=18102]') = 1;

xquery
<total>{
  sum (for $p in collection("oradb:/ZSBD_W1/PARTICIPANTSXML/ROW/PARTICIPANTS")/participants/participant
     where $p/@userId = "18102"
     return sum(number($p/soldticket/@price)))
}</total>
    

-- 3 news messages created by specific user

select news.* from news where authorid = 2122;

select news.* from news 
where 2048 <= news.eventid and news.eventId < 4096 and news.authorid = 2122;
  
select * from XmlTable(
  'for $ns in collection("oradb:/ZSBD_W1/EVENTS/ROW/NEWS")/news[newsEntry/authorId = 2122]
   where $ns/@eventId >= 2048 and $ns/@eventId < 4096
   return <news id="{$ns/@eventId}">{
            for $entry in $ns/newsEntry[authorId = 2122]
            return $entry
   }</news>'
);

select news.message from news where authorid = 11155;

Create Or Replace Procedure readNews As
   doc          Dbms_Xmldom.Domdocument;
   ndoc			    Dbms_xmldom.DOMNode;
   elem         Dbms_Xmldom.Domelement;
   nodeList     Dbms_Xmldom.Domnodelist;
   node         Dbms_Xmldom.Domnode;
   childList    Dbms_Xmldom.Domnodelist;
   childAuthor  Dbms_Xmldom.Domnode;
   childMessage Dbms_Xmldom.Domnode;
   textNode     Dbms_Xmldom.Domnode;
   buf          varchar2(2000);
   authorVal    varchar2(100);
   len          number;
Begin
   FOR var IN (select news from events) LOOP
   doc     := dbms_xmldom.newDOMDocument(var.news); 
   ndoc    := dbms_xmldom.makeNode(doc);

   dbms_xmldom.writetobuffer(ndoc, buf);

   elem := dbms_xmldom.getDocumentElement( doc );
   
   nodelist := dbms_xmldom.getchildrenbytagname(elem, 'newsEntry');
   len  := DBMS_XMLDOM.getLength(nodelist);
   
   for i in 0..len-1 loop
		node := dbms_xmldom.item(nodelist, i);
    childList := dbms_xmldom.getChildNodes(node);
    childAuthor := dbms_xmldom.item(childList, 1);
    authorVal := xmldom.getNodeValue(dbms_xmldom.getfirstchild(childAuthor));
		if(authorVal = '11155') THEN
      childMessage := dbms_xmldom.item(childList, 3);
			textNode := dbms_xmldom.getfirstchild(childMessage);
			dbms_output.put_line('Message: '|| xmldom.getNodeValue(textNode));
		End if;
   end loop;
   xmldom.freeDocument(doc);
   END LOOP;
End;
/

Create Or Replace Procedure readNews_cl As
   doc          Dbms_Xmldom.Domdocument;
   ndoc			    Dbms_xmldom.DOMNode;
   elem         Dbms_Xmldom.Domelement;
   nodeList     Dbms_Xmldom.Domnodelist;
   node         Dbms_Xmldom.Domnode;
   childList    Dbms_Xmldom.Domnodelist;
   childAuthor  Dbms_Xmldom.Domnode;
   childMessage Dbms_Xmldom.Domnode;
   textNode     Dbms_Xmldom.Domnode;
   buf          varchar2(2000);
   authorVal    varchar2(100);
   len          number;
Begin
   FOR var IN (select news_cl from events) LOOP
   doc     := dbms_xmldom.newDOMDocument(var.news_cl); 
   ndoc    := dbms_xmldom.makeNode(doc);

   dbms_xmldom.writetobuffer(ndoc, buf);

   elem := dbms_xmldom.getDocumentElement( doc );
   
   nodelist := dbms_xmldom.getchildrenbytagname(elem, 'newsEntry');
   len  := DBMS_XMLDOM.getLength(nodelist);
   
   for i in 0..len-1 loop
		node := dbms_xmldom.item(nodelist, i);
    childList := dbms_xmldom.getChildNodes(node);
    childAuthor := dbms_xmldom.item(childList, 1);
    authorVal := xmldom.getNodeValue(dbms_xmldom.getfirstchild(childAuthor));
		if(authorVal = '11155') THEN
      childMessage := dbms_xmldom.item(childList, 3);
			textNode := dbms_xmldom.getfirstchild(childMessage);
			dbms_output.put_line('Message: '|| xmldom.getNodeValue(textNode));
		End if;
   end loop;
   xmldom.freeDocument(doc);
   END LOOP;
End;
/

begin
  readNews;
end;
/

begin
  readNews_cl;
end;
/

select extractValue(e.news, '//message')
from Events e
where existsNode(e.news, '//authorId[text()="11155"]') = 1;

select extractValue(e.news_cl, '//message')
from Events e
where existsNode(e.news_cl, '//authorId[text()="11155"]') = 1;


-- 4 add coordinated event for a user

insert into coordinators values (12, 2, 'speaker');

Create Or Replace Procedure createUser As
   e_elem     xmldom.DomElement;
   doc        Dbms_Xmldom.Domdocument;
   ndoc			  dbms_xmldom.DOMNode;
   elem       Dbms_Xmldom.Domelement;
   nodeList   Dbms_Xmldom.Domnodelist;
   node       Dbms_Xmldom.Domnode;
   event_node Dbms_Xmldom.Domnode;
   e_node     Dbms_Xmldom.Domnode;
   buf        varchar2(2000);
   item_text  xmldom.DOMText;
   clob_res   clob;
   xml_res    xmltype;

BEGIN
  FOR user IN (select userinfo from users where id = 2) LOOP
   DBMS_LOB.CreateTemporary(clob_res, TRUE);
   doc     := dbms_xmldom.newDOMDocument(user.userinfo); 
   ndoc    := dbms_xmldom.makeNode(doc);

   dbms_xmldom.writetobuffer(ndoc, buf);

   elem := dbms_xmldom.getDocumentElement( doc );
   
   nodelist := dbms_xmldom.getchildrenbytagname(elem, 'coordinatedEvents');
   node := dbms_xmldom.item(nodelist, 0);
   FOR event IN (select * from events where id = 12) LOOP
      e_elem := xmldom.createElement(doc , 'event' );
      event_node := xmldom.appendChild( node , xmldom.makeNode(e_elem) );
      
      e_elem := xmldom.createElement(doc , 'id' );
      e_node := xmldom.appendChild( event_node , xmldom.makeNode(e_elem) );
      item_text := xmldom.createTextNode( doc , event.id );
      e_node := xmldom.appendChild( e_node , xmldom.makeNode(item_text) );
      
      e_elem := xmldom.createElement(doc , 'title' );
      e_node := xmldom.appendChild( event_node , xmldom.makeNode(e_elem) );
      item_text := xmldom.createTextNode( doc , event.title );
      e_node := xmldom.appendChild( e_node , xmldom.makeNode(item_text) );
      
      e_elem := xmldom.createElement(doc , 'date' );
      e_node := xmldom.appendChild( event_node , xmldom.makeNode(e_elem) );
      item_text := xmldom.createTextNode( doc , event.eventdatetime );
      e_node := xmldom.appendChild( e_node , xmldom.makeNode(item_text) );
      
      e_elem := xmldom.createElement(doc , 'role' );
      e_node := xmldom.appendChild( event_node , xmldom.makeNode(e_elem) );
      item_text := xmldom.createTextNode( doc , 'speaker' );
      e_node := xmldom.appendChild( e_node , xmldom.makeNode(item_text) );
  END LOOP;
  dbms_xmldom.writetobuffer(ndoc, buf);
  dbms_output.put_line( buf);
  xmldom.writetoclob(xmldom.makenode(elem), clob_res, 'UTF-8');
  xml_res := xmltype.createxml(clob_res);
  xmldom.freeDocument(doc);
  
  update users set userinfo = xml_res where id = 2;
  END LOOP;
END;


begin
  createUser;
end;

-- 5 add new coordinator (new user)

create or replace function fakeCoordinator
  return XmlType 
is
  theResult XmlType;
begin
  select XmlElement(name "coordinator",
    XmlAttributes(2 as "userId"),
    XmlElement(name "name", users.name),
    XmlElement(name "role", 'speaker'),
    XmlElement(name "fake"))
  into theResult from users where users.id = 2;
  return theResult;
end;
/

-- the default xml storage
begin
  update events
  set coordinators = 
    appendChildXml(coordinators, '//coordinators', fakeCoordinator())
  where Id = 264; 

  UPDATE events
  SET coordinators=DELETEXML(coordinators,'//coordinators/coordinator[@userId=2]')
  WHERE id = 264;
end;
/

-- the varchar xml storage
begin
  update events
  set coordinators_vc = XmlSerialize(content
    appendChildXml(XmlParse(document coordinators_vc wellformed), '//coordinators', fakeCoordinator())
  ) where Id = 264; 

  UPDATE events
  SET coordinators_vc = XmlSerialize(content
    DELETEXML(XmlParse(document coordinators_vc wellformed),'//coordinators/coordinator[@userId=2]')
  ) WHERE id = 264;
end;
/

select coordinators_vc, coordinators from events where id = 264;

begin
  insert into users(id, login, email, passwordsalt, passwordhash) 
    values (1000001, 'some_login', 'some_email', 'foo', 'bar');
  insert into coordinators(eventid, userid) values (42, 1000001);
  delete from coordinators where eventid = 42 and userid = 1000001;
  delete from users where id = 1000001;
end;
/


