-- registering the schema creates a bunch of types, indexes and system tables

--------------------------------------------------------------------------------
create table UserInfoXt of XmlType
XmlType store as clob
xmlschema "USER-USERINFO" element "userInfo";

insert into UserInfoXt
select XmlElement(
  name "userInfo",
  XmlForest(u.id as "id", u.name as "name", 
            u.surname as "surname", u.email as "email"),
  XmlElement(name "city", XmlAttributes(r.country as "country"), r.city),
  XmlElement(name "membership"),
  XmlElement(name "coordinatedEvents")
) 
from Users u join Regions r on u.defaultRegion = r.id
;
-- this has created only the xmltable


--select * from UserInfoXt;

-- this is slower than access to XmlType column (?) : 5s
select extractValue(value(u), '//name/text()') as name,
       extractValue(value(u), '//surname/text()') as surname,
       extractValue(value(u), '//city/text()') as city
from UserInfoXt u
where existsNode(OBJECT_VALUE, '//city[@country = "Denmark"]') = 1;

--------------------------------------------------------------------------------
create table UserInfoOr of XmlType
XmlType store as object relational
xmlschema "USER-USERINFO" element "userInfo";
-- this has created some system (gibberishly named) tables

insert into UserInfoOr
select XmlElement(
  name "userInfo",
  XmlForest(u.id as "id", u.name as "name", 
            u.surname as "surname", u.email as "email"),
  XmlElement(name "city", XmlAttributes(r.country as "country"), r.city),
  XmlElement(name "membership"),
  XmlElement(name "coordinatedEvents")
) 
from Users u join Regions r on u.defaultRegion = r.id
;

-- foo!
select value(u).getStringVal() from UserInfoOr u;

-- now i can do this:
select u.XMLDATA."id", u.XMLDATA."email", u.XMLDATA."name"
from UserInfoOr u
where u.XMLDATA."name" like 'N%';

-- what about the previous query? seems faster! 0.523s / 0.238s
select u.XMLDATA."id", u.XMLDATA."name", u.XMLDATA."surname", 
      u.XMLDATA."city"."SYS_XDBBODY$"
from UserInfoOr u
where u.XMLDATA."city"."country" = 'Denmark';

-- can do xml as well: 0.267s
select extractValue(value(u), '//name/text()') as name,
       extractValue(value(u), '//surname/text()') as surname,
       extractValue(value(u), '//city/text()') as city
from UserInfoOr u
where existsNode(OBJECT_VALUE, '//city[@country = "Denmark"]') = 1;
