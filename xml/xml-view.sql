create or replace view UserInfoXmlView of XmlType
with object id (extract(OBJECT_VALUE, '/userInfo/id').getNumberVal())
as
select 
  XmlElement(
    name "userInfo",
    XmlForest(
      u.id as "id",
      u.name as "name",
      u.surname as "surname",
      u.email as "email"
    ),
    XmlElement(name "city", XmlAttributes(r.country as "country"), r.city),
    XmlElement(name "memberships",
      (select XmlAgg(
         XmlElement(name "group",
           XmlAttributes(m1.groupId as "id", g1.name as "name")
       ))
       from Memberships m1 join Groups g1 on g1.id = m1.groupId
       where m1.userid = u.id)
    ),
    XmlElement(name "coordinatedEvents",
      (select XmlAgg(
         XmlElement(name "event",
           XmlForest(
             e2.id as "id",
             e2.title as "title",
             e2.eventDateTime as "date",
             c2.role as "role"
           )
         )
       )
       from Coordinators c2 join Events e2 on c2.eventId = e2.id 
       where c2.userId = u.id)
    )
  )
from Users u
  join Regions r on u.defaultRegion = r.id
;

create or replace view UserInfoSimpleXmlView of XmlType
with object id (extract(OBJECT_VALUE, '/userInfo/id').getNumberVal())
as
select 
  XmlElement(
    name "userInfo",
    XmlForest(
      u.id as "id",
      u.name as "name",
      u.surname as "surname",
      u.email as "email"
    ),
    XmlElement(name "city", XmlAttributes(r.country as "country"), r.city)
  )
from Users u
  join Regions r on u.defaultRegion = r.id
;

-- sample query
-- takes *very* long, as this is basically reconstructing 
-- what we have in the table
select extractValue(value(u), '//name/text()') as name,
       extractValue(value(u), '//surname/text()') as surname,
       extractValue(value(u), '//city/text()') as city
from UserInfoXmlView u
where u.existsNode('//city[@country = "Denmark"]') = 1;

-- let's try with a simpler one. better, but still slow.
-- 3.873s
select extractValue(value(u), '//name/text()') as name,
       extractValue(value(u), '//surname/text()') as surname,
       extractValue(value(u), '//city/text()') as city
from UserInfoSimpleXmlView u
where u.existsNode('//city[@country = "Denmark"]') = 1;

-- compare execution time with Users.userInfo
-- this is faster: 0.719s
select extractValue(u.userInfo, '//name/text()') as name,
       extractValue(u.userInfo, '//surname/text()') as surname,
       extractValue(u.userInfo, '//city/text()') as city
from Users u
where u.userInfo.existsNode('//city[@country = "Denmark"]') = 1;

-- same (almost) but with xquery: 0.789s
select * from XmlTable(
  'for $u in fn:collection("oradb:/ZSBD_W1/USERS/ROW/USERINFO")/userInfo
   where $u/city/@country = "Denmark"
   return <user id="{$u/id/text()}">
            <name>{$u/name/text()}{" "}{$u/surname/text()}</name>
            <city>{$u/city/text()}</city>
          </user>'
);
