drop index cities_locations_ix;
drop index countries_polygons_ix;
drop index groups_areas_ix;

delete from USER_SDO_GEOM_METADATA where TABLE_NAME = 'COUNTRIES' and COLUMN_NAME = 'POLYGON';
delete from USER_SDO_GEOM_METADATA where TABLE_NAME = 'GROUPS' and COLUMN_NAME = 'AREA';
delete from USER_SDO_GEOM_METADATA where TABLE_NAME = 'REGIONS' and COLUMN_NAME = 'CITYLOCATION';