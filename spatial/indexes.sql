INSERT INTO USER_SDO_GEOM_METADATA 
VALUES (
  'COUNTRIES',
  'POLYGON',
  MDSYS.SDO_DIM_ARRAY(
    MDSYS.SDO_DIM_ELEMENT('X', -180.0, 180.0, 1),
    MDSYS.SDO_DIM_ELEMENT('Y', -90.0, 90.0, 1)),
  8307
);
create index countries_polygons_ix on countries(polygon) indextype is mdsys.spatial_index;

INSERT INTO USER_SDO_GEOM_METADATA 
VALUES (
  'GROUPS',
  'AREA',
  MDSYS.SDO_DIM_ARRAY(
    MDSYS.SDO_DIM_ELEMENT('X', -180.0, 180.0, 1),
    MDSYS.SDO_DIM_ELEMENT('Y', -90.0, 90.0, 1)),
  8307
);
create index groups_areas_ix on groups(area) indextype is mdsys.spatial_index;

INSERT INTO USER_SDO_GEOM_METADATA 
VALUES (
  'REGIONS',
  'CITYLOCATION',
  MDSYS.SDO_DIM_ARRAY(
    MDSYS.SDO_DIM_ELEMENT('X', -180.0, 180.0, 1),
    MDSYS.SDO_DIM_ELEMENT('Y', -90.0, 90.0, 1)),
  8307
);
create index cities_locations_ix on regions(citylocation) indextype is mdsys.spatial_index;