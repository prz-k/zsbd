-- 1 - znajduje te w środku, a nie na zewnątrz
SELECT distinct users.name, r.city, r2.city
FROM users left join regions r on users.defaultregion = r.id, groups left join regions r2 on groups.regionid = r2.id, memberships
WHERE memberships.userid = users.id and memberships.groupid = groups.id and SDO_RELATE(r.citylocation, groups.area,
'mask=INSIDE querytype=JOIN') = 'TRUE';

-- nie chcielismy przypadkiem czegos takiego? ZP: tak :)
SELECT distinct users.id, users.name, users.surname
FROM users left join regions r on users.defaultregion = r.id, 
     groups, memberships
WHERE memberships.userid = users.id and memberships.groupid = groups.id 
  and SDO_RELATE(r.citylocation, groups.area, 'mask=DISJOINT querytype=JOIN') = 'TRUE';

-- odpowiednik 'sql-only'
select distinct u.id, u.name, u.surname
from Users u
  left join Regions r on u.defaultRegion = r.id
  left join Memberships m on m.userId = u.id
  left join Groups g on g.id = m.groupId
  left join Regions rg on g.regionId = rg.id
where g.id is not null
  and r.id <> rg.id;

-- 2

insert into Memberships (userId, groupId) 
(select u.id, g.id
from Groups g, Users u, Regions r
where 1920 <= u.id and u.id < 2176 
  and u.defaultRegion = r.id
  and sdo_contains(g.area, r.cityLocation) = 'TRUE'
);

insert into Memberships (userId, groupId) 
(select u.id, g.id
from Users u, Groups g  
where 1920 <= u.id and u.id < 2176
  and g.regionId = u.defaultRegion
);

-- 3

SELECT events.title, r1.city as eventCity, r2.city as userCity
FROM events left join REGIONS r1 on events.regionid = r1.id,
users left join regions r2 on users.defaultregion = r2.id
WHERE users.id = 2 and r1.city <> r2.city
   AND sdo_within_distance (
        r2.citylocation,  r1.citylocation,
        'distance=100 unit=km') = 'TRUE';

select e.id, re.city as eventCity, ru.city as userCity
from Events e, Regions re, Users u, Regions ru
where u.id = 2
  and u.defaultRegion = ru.id
  and (re.country = ru.country and re.province = ru.province)
  and re.id = e.regionId;

-- 4
-- 41 to Polska, 22 - Niemcy Łączymy polygon Polski i Niemiec i wstawiamy do Polski
-- powierzchnia przed: 322764 powierzchnia po: 704211

update countries 
set polygon = (SELECT SDO_GEOM.SDO_UNION(c1.polygon, c2.polygon, 2)
from countries c1, countries c2
where c1.id = 41 and c2.id = 22) where id  = 41; 	

-- 5
SELECT events.title, r1.city as eventCity, r1.country, r2.city as userCity
FROM events left join REGIONS r1 on events.regionid = r1.id left join countries on countries.id = r1.COUNTRYID,
regions r2 
WHERE r2.city = 'Berlin' 
AND sdo_within_distance ( r2.citylocation,  r1.citylocation,
'distance=500 unit=km') = 'TRUE'
and SDO_RELATE(r2.citylocation, countries.polygon,
'mask=OUTSIDE querytype=JOIN') = 'TRUE'
;

-- to jakos dziala mi szybciej od zapytania wyzej, ale nadal dlugo (25s!)
select r1.city as srcCity, r2.city as evtCity, e.id, e.title
from Regions r1, Regions r2, Countries c, Events e
where r1.city = 'Berlin'
  and c.id = r1.countryId
  and sdo_relate(r2.cityLocation, c.polygon, 'mask=DISJOINT') = 'TRUE'
  and sdo_within_distance(r2.cityLocation, r1.cityLocation, 'distance=500 unit=km') = 'TRUE'
  and e.regionId = r2.id;

select e.title, r1.city
from Events e, Regions r1, Regions r2
where r2.city = 'Berlin'
  and r1.country <> r2.country
  and e.regionId = r1.id;

-- 6

-- wariant 1
select r1.city, r2.city,
       round(sdo_geom.sdo_distance(r1.citylocation, r2.citylocation, 10, 'unit=km')) as distance,
       u.id, u.name, u.surname
from Regions r1, Regions r2, Users u
where r1.city like 'Palermo%'
  and sdo_within_distance(r1.cityLocation, r2.cityLocation, 'distance=250 unit=km') = 'TRUE'
  and r2.id <> r1.id
  and r2.id = u.defaultRegion;
  
-- wariant 2
select r1.city, r2.city, round(sdo_nn_distance(1)) as distance,
       u.id, u.name, u.surname
from Regions r1, Regions r2, Users u
where r1.city = 'Palermo'
  and r2.id <> r1.id
  and sdo_nn(r2.cityLocation, r1.cityLocation, 'sdo_num_res=5 distance=250 unit=km', 1) = 'TRUE'
  and u.defaultRegion = r2.id;
  
-- powiedzmy, ze 'sasiednie' to 'w tej samej prowincji'
select r1.city, r2.city,'???' as distance,
       u.id, u.name, u.surname
from Regions r1, Regions r2, Users u
where r1.city like 'Palermo%'
  and r1.province = r2.province
  and r2.id <> r1.id
  and r2.id = u.defaultRegion;
  
-- 7
-- na 11g nie mam OFFSET/FETCH...
select * from (
  select name, sdo_geom.sdo_area(polygon, 10, 'unit=SQ_KM') as area
  from Countries
  order by 2
) where ROWNUM = 1;

-- bonus:
select sdo_aggr_convexhull(sdoaggrtype(r.cityLocation, 0.5))
from Regions r, Regions r1
where r1.city = 'Katowice'
  and sdo_within_distance(r1.cityLocation, r.cityLocation, 'distance=30 unit=km') = 'TRUE';

create or replace function ConvexHullForCity(cityName in varchar, kms number)
return sdo_geometry 
is
  hull sdo_geometry;
begin
  select sdo_aggr_convexhull(sdoaggrtype(r.cityLocation, 0.5))
  into hull
  from Regions r, Regions r1
  where r1.city = cityName
    and sdo_within_distance(r1.cityLocation, r.cityLocation, 'distance=' || kms || ' unit=km') = 'TRUE';
  return(hull);
end;
/

select r1.city, ConvexHullForCity(r1.city, 30)
from Regions r1, Regions r2
where sdo_nn(r1.cityLocation, r2.cityLocation, 'distance=30 unit=km', 1) = 'TRUE'
group by r1.city
having count(*) >= 10;
  
-- mierzy powierzchnię kraju o id 41 (Polska)
select name,
ROUND(SDO_GEOM.sdo_area(polygon, 1, 'unit=SQ_KM')) POWIERZCHNIA  
from countries where id = 41;

-- Znajduje kraje, których powierzchnie na siebie nachodzą lub się stykają (Polska vs Niemcy i Czechy)
select c1.name, c1.polygon, c2.name, c2.polygon
from countries c1, countries c2
where c1.id <> c2.id and c1.id = 41 and c2 IN (13, 22) and SDO_RELATE(c1.polygon, c2.polygon,
'mask=OVERLAPBDYINTERSECT or TOUCH, querytype=JOIN') = 'TRUE';	