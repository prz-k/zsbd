-- 1 star

-- 1

CREATE MATERIALIZED VIEW
SALES_INFO_MV
BUILD IMMEDIATE
REFRESH COMPLETE
ENABLE QUERY REWRITE AS
SELECT TICKETID, sum(ticketvalue) AS EXPECTED_VALUE, sum(ticketprice) AS REAL_VALUE, count(1) AS TICKETS_COUNT
FROM SALES
GROUP BY TICKETID;

-- using MV
select * from SALES_INFO_MV;

-- using query
select t.id, sum(t.defaultprice), sum(s.price), count(1)
from tickets t, soldtickets s where t.id = s.ticketid group by t.id;

-- 2

CREATE MATERIALIZED VIEW
PRICE_DIFFERENCE_MV
BUILD IMMEDIATE
REFRESH COMPLETE
ENABLE QUERY REWRITE AS
SELECT TICKETID, ticketvalue - ticketprice AS PRICE_DIFFERENCE
FROM SALES;

select * from price_difference_mv;

select t.id, t.defaultprice-s.price
from tickets t, soldtickets s where t.id = s.ticketid;

select ticketid, sum(price_difference) AS TOTAL_DIFFERENCE from price_difference_mv GROUP BY ticketid;

-- 3 sale statistics per month
SELECT EVENTID, MONTH_ID as MONTH, sum(ticketprice) AS MAX_INCOME
FROM SALES s, SalesTimes st where  s.day_id = st.day_id
GROUP BY EVENTID, st.MONTH_ID
order by eventid, MAX_INCOME desc;

-- avg number of tickets for events per region
select e.regionid, avg(s.ticketcount)
from sales s, events e where e.id = s.eventid
group by e.REGIONID;

-- 2 star

-- 1

CREATE MATERIALIZED VIEW
PARTICIPANTS_PER_COUNTRY_AND_YEAR_MV
BUILD IMMEDIATE
REFRESH COMPLETE
ENABLE QUERY REWRITE AS
SELECT r.country, p.year_id, count(*) AS participants_count from cube_Participants c, ParticipantsTimes p, Regions r
where c.day_id = p.DAY_ID and r.ID = c.REGION_ID
GROUP BY r.country, p.YEAR_ID
order by r.country, p.year_id;

select * from PARTICIPANTS_PER_COUNTRY_AND_YEAR_MV;

-- 2

CREATE MATERIALIZED VIEW
EVENTS_PER_COUNTRY_AND_YEAR_MV
BUILD IMMEDIATE
REFRESH COMPLETE
ENABLE QUERY REWRITE AS
SELECT r.country, p.year_id, count(distinct event_id) AS event_count from cube_Participants c, ParticipantsTimes p, Regions r
where c.day_id = p.DAY_ID and r.ID = c.REGION_ID
GROUP BY r.country, p.YEAR_ID
order by r.country, p.year_id;

select * from EVENTS_PER_COUNTRY_AND_YEAR_MV;

-- in which month there are most events
select pt.month_num, count(cp.event_id) as EVENTS_COUNT
FROM cube_participants cp, ParticipantsTimes pt 
where cp.DAY_ID = pt.DAY_ID
group by pt.month_num
order by events_count desc;