-- ??? --
drop dimension dim_User;
create dimension dim_User level user_id is Users.id 
  attribute user_id determines (Users.name, Users.surname, Users.email, Users.defaultRegion);
drop dimension dim_Event;
create dimension dim_Event level event_id is Events.id 
  attribute event_id determines (Events.title, Events.eventDateTime, Events.regionId);
drop dimension dim_Ticket;
create dimension dim_ticket level ticket is Tickets.id
  attribute ticket determines (Tickets.defaultprice, Tickets.count);
create dimension dim_Organizer level organizer is Groups.id
  attribute organizer determines (Groups.name);
execute dbms_dimension.validate_dimension('dim_User', false, true, 'dim_User');
execute dbms_dimension.validate_dimension('dim_Event', false, true, 'dim_Event');
execute dbms_dimension.validate_dimension('dim_Ticket', false, true, 'dim_Ticket');
execute dbms_dimension.validate_dimension('dim_Organizer', false, true, 'dim_Organizer');
select * from dimension_exceptions;

--------------------------------------------------------------------------------
-- 1
--------------------------------------------------------------------------------

drop table Sales;
CREATE TABLE SALES
(
  day_id date,
  SOLDTICKEITID NUMBER,
  TICKETID NUMBER,
  EVENTID NUMBER,
  ORGANIZERID NUMBER,
  TICKETVALUE NUMBER,
  TICKETPRICE NUMBER,
  TICKETCOUNT NUMBER,
  
  CONSTRAINT fk_timeid FOREIGN KEY (day_id) references SalesTimes(day_id),
  CONSTRAINT fk_ticketid FOREIGN KEY (ticketid) references tickets(id),
  CONSTRAINT fk_soldticketid FOREIGN KEY (soldtickeitid) references soldtickets(id),
  CONSTRAINT fk_eventid FOREIGN KEY (EVENTID) references events(id),
  CONSTRAINT fk_organizerid FOREIGN KEY (organizerid) references groups(id)  
);

delete from Sales;
INSERT INTO SALES
SELECT s.purchasedate, s.id, t.id, e.id, e.organizerid, t.defaultprice, s.price, t.count
FROM soldtickets s, tickets t, events e
where s.TICKETID = t.id and t.eventid = e.id;

drop table SalesTimes;
create table SalesTimes (
  day_id date,
  month_num number,
  month_id char(7), -- mm-yyyy
  year_id number,
  primary key(day_id)
);

delete from SalesTimes;
insert into SalesTimes (day_id, month_num, month_id, year_id) (
  select distinct purchasedate,
                  extract(month from purchasedate),
                  to_char(purchasedate, 'MM-YYYY'),
                  extract(year from purchasedate)
  from SoldTickets
);

drop dimension dim_SalesTimes;
create dimension dim_SalesTimes
  level day is SalesTimes.day_id
  level month is SalesTimes.month_id
  level year is SalesTimes.year_id
  hierarchy h_SalesTimes (day child of month child of year)
  attribute day determines (SalesTimes.month_id, SalesTimes.year_id)
  attribute month determines (SalesTimes.year_id);

-- check
execute dbms_dimension.validate_dimension('dim_SalesTimes', false, true, 'dim_SalesTimes01');
select * from dimension_exceptions where statement_id = 'dim_SalesTimes01';
delete from dimension_exceptions where statement_id = 'dim_SalesTimes01';

--------------------------------------------------------------------------------
-- 2
--------------------------------------------------------------------------------

drop table ParticipantsTimes;
create table ParticipantsTimes (
  day_id date,
  month_num number,
  month_id char(7), -- mm-yyyy
  year_id number,
  primary key(day_id)
);

delete from ParticipantsTimes;
insert into ParticipantsTimes (day_id, month_num, month_id, year_id) (
  select distinct to_date(to_char(eventdatetime, 'DD-MM-YYY'), 'DD-MM-YYY'),
                  extract(month from eventdatetime),
                  to_char(eventdatetime, 'MM-YYYY'),
                  extract(year from eventdatetime)
  from Events
);

drop dimension dim_ParticipantsTimes;
create dimension dim_ParticipantsTimes
  level day is ParticipantsTimes.day_id
  level month is ParticipantsTimes.month_id
  level year is ParticipantsTimes.year_id
  hierarchy h_ParticipantsTimes (day child of month child of year)
  attribute day determines (ParticipantsTimes.month_id, ParticipantsTimes.year_id)
  attribute month determines (ParticipantsTimes.year_id);

execute dbms_dimension.validate_dimension('dim_ParticipantsTimes', false, true, 'dim_ParticipantsTimes01');
select * from dimension_exceptions where statement_id = 'dim_ParticipantsTimes01';
delete from dimension_exceptions where statement_id = 'dim_ParticipantsTimes01';

drop dimension dim_Region;
create dimension dim_Region
  level city is Regions.id
  level province is Regions.province
  level country is Regions.countryId
  hierarchy h_Region (city child of province child of country)
  attribute city determines (Regions.province, Regions.country);

execute dbms_dimension.validate_dimension('dim_Region', false, true, 'dim_Region01');
select * from dimension_exceptions where statement_id = 'dim_Region01';
delete from dimension_exceptions where statement_id = 'dim_Region01';

create table cube_Participants (
  event_id number,
  day_id date,
  region_id number,
  user_id number,
  indicator number,
  
  constraint fk_event foreign key (event_id) references Events(id),
  constraint fk_date foreign key (day_id) references ParticipantsTimes(day_id),
  constraint fk_region foreign key (region_id) references Regions(id),
  constraint fk_user foreign key (user_id) references Users(id)
);

insert into cube_Participants(event_id, day_id, region_id, user_id, indicator) (
  select e.id, to_date(to_char(eventdatetime, 'DD-MM-YYY'), 'DD-MM-YYY'),
         r.id, p.userId, 1
  from Events e
    join Regions r on r.id = e.regionId
    join Participants p on p.eventId = e.id
);




