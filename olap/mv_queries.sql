--
-- for query 1
--

-- MV: ticket sales by organizer by month (name length limit...)
drop materialized view ticketSales_orgMnt;
create materialized view ticketSales_orgMnt
build immediate
refresh complete
enable query rewrite
as
  select s.organizerId, st.month_id,
         sum(ticketvalue) as totalValue, avg(ticketvalue) as avgValue, 
         sum(ticketprice) as totalPrice, avg(ticketprice) as avgPrice,
         sum(ticketcount) as totalCount, avg(ticketcount) as avgCount
  from Sales s, SalesTimes st
  where st.day_id = s.day_id
  group by s.organizerid, st.month_id;
  
select * 
from ticketSales_orgMnt
where to_date(month_id, 'MM-YYYY') >= to_date('02-2012', 'MM-YYYY')
  and to_date(month_id, 'MM-YYYY') <= to_date('07-2012', 'MM-YYYY')
order by to_date(month_id, 'MM-YYYY') asc, totalValue desc;

-- shows query rewrite in execution plan!
select g.name, s.organizerId, st.month_id, sum(s.ticketValue) 
from Sales s, SalesTimes st, Groups g
where st.day_id = s.day_id
  and g.id = s.organizerId
  and st.month_id = '02-2012'
group by g.name, s.organizerId, st.month_id
order by 4 desc;
