
alter system flush buffer_cache;
alter system flush shared_pool;

timing start script_time
set autotrace traceonly 
spool B_write_result.out


insert into groups(name, description, createdby, createdate, iscompany)
  select 'Special groupname', 'Description 21324 Description', 
         uids, to_date('02-02-02', 'DD-MM-YY'), 0
  from (select uids 
        from (select users.id uids 
              from users 
              where id not in (select createdby from groups)) 
  where rownum <=1);


insert into memberships(userid, groupid)
  select uids, gids 
  from (select uids 
        from (select users.id uids 
              from users 
              where id not in (select userid from memberships) 
              group by defaultregion, id) 
        where rownum <=30),
  (select max(id) gids from groups);


update tickets
set DEFAULTPRICE = DEFAULTPRICE * 1.1
where (select count(*) soldT 
       from soldtickets 
       where ticketid = id) < tickets.count;


update events
set title = concat(title, id)
where (title, regionid) in 
  (select title, regionid 
   from events 
   group by title, REGIONID 
   having count(*) > 1);


insert into memberships(userid, groupid)
  select uids, gid 
  from (select uids 
        from (select id uids 
              from users 
              where id not in 
                (select userid from memberships)) 
        where ROWNUM <=10),
  (select groupid gid 
   from (select groupid 
         from memberships 
           join groups on (memberships.groupid = groups.id)
   where iscompany = 0 group by groupid));


delete from soldtickets
where price < (select defaultprice 
               from tickets 
               where tickets.id = soldtickets.id);


--delete from participants
--where id not in 
--  (select participantid 
--   from soldtickets 
--   where purchasedate > to_date('01-01-14', 'DD-MM-YY'));


timing stop script_time
spool off
exit
