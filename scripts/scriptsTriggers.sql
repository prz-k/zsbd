-----
-- 1 a naruszenie
insert into events 
select max(id)+1, 'added event', null, null, current_date - 1, current_date, null, null, 1 from events;

update events
set eventdatetime = eventdatetime -1 where id = 2;

----
-- 3 insert

insert into soldtickets(ticketid, participantid, purchasedate, price)
  values( 19748, 871, null, 100);

-- 3 rollback
insert into tickets(id, eventid, count, defaultprice, description)
values(1234567, null, 0, 2, null);

insert into soldtickets(ticketid, participantid, purchasedate, price)
  values(1234567, 871, current_date, 100 );
  
update tickets
set count = 1 where id = 203;

-- 4 nie działa
insert into soldtickets(ticketid, participantid)
select tickets.id, 817 from tickets, events
where tickets.eventid = events.id and events.EVENTDATETIME < current_date;

insert into soldtickets(ticketid, participantid)
values(13, 817); 