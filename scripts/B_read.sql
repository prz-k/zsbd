alter system flush buffer_cache;
alter system flush shared_pool;

timing start script_time
set autotrace traceonly 
spool B_read_result.out


SELECT * 
FROM participants 
  LEFT JOIN soldtickets ON (soldtickets.participantid = participants.id) 
  LEFT JOIN users ON (participants.userid = users.id) 
  LEFT JOIN events ON (events.id = participants.eventid) 
WHERE eventid IN 
  (SELECT eventid 
   FROM (SELECT eventid, count(*) 
         FROM tickets 
         group by eventid 
         HAVING count(*) = 2)) 
ORDER BY participants.eventid, users.surname, users.name;


SELECT *
FROM participants 
  JOIN users ON (participants.userid = users.id) 
WHERE userid NOT IN 
  (SELECT userid 
   FROM participants 
     JOIN users ON (participants.userid = users.id) 
     INNER JOIN (SELECT events.* FROM events) ON (regionid = users.defaultregion) 
  WHERE eventid IN 
    (SELECT id 
     FROM events 
     WHERE organizerid = (SELECT groupid 
                          FROM memberships 
                          GROUP BY groupid 
                          HAVING COUNT(*) = (SELECT MAX(COUNT(*)) 
                                             FROM memberships 
                                             GROUP BY groupid))));


SELECT * 
FROM participants 
WHERE participants.eventid NOT IN
  (SELECT eventid 
   FROM (SELECT eventid, SUM(tickets.count) 
         FROM tickets 
         GROUP BY eventid) 
   WHERE ROWNUM <=5); 


SELECT avg(soldtickets.participantid) 
FROM soldtickets 
  JOIN tickets ON (soldtickets.ticketid = tickets.id) 
  JOIN events ON (events.id = tickets.eventid)
WHERE events.organizerid IN (SELECT groups.id FROM groups)
GROUP BY events.id;


SELECT events.id, MAX(purchasedate) - MIN(purchasedate) 
FROM events 
  JOIN tickets ON tickets.eventid = events.id 
  JOIN soldtickets ON soldtickets.ticketid = tickets.id 
GROUP BY events.id 
ORDER BY 2;


timing stop script_time
spool off
exit