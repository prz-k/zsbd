alter system flush buffer_cache;
alter system flush shared_pool;


timing start script_time
set autotrace traceonly
spool A_write_result.out


insert into participants(eventid, userid)
  select idE, userid 
  from (select id idE 
        from (select events.id 
              from events 
              where regionid = 
                (select defaultregion 
                 from (select defaultregion 
                       from users 
                       group by defaultregion 
                       having count(*) = 
                         (select min(count(defaultregion)) 
                          from users 
                          group by defaultregion))
                 where ROWNUM <=1) 
              and creationdate > to_date('14-01-11', 'DD-MM-YY')) 
        where ROWNUM<=1), 
  (select userid 
  from (select id userid 
        from users 
        where id not in 
          (select userid from memberships)) 
  where ROWNUM <=20);


insert into soldtickets(ticketid, participantid, purchasedate, price)
  select ticketid, participantid, null, 100 
  from (select max(id) ticketid 
        from tickets 
        where eventid = 
          (select eventid 
          from participants, events
          where events.id = participants.eventid and events.eventdatetime > current_date and rownum = 1)),
  (select participantid 
  from (select id participantid 
        from participants 
        order by id DESC) 
  where ROWNUM <=20);


update users
set passwordhash = 'ndoiewhdowehdow3345u89', passwordsalt='KHbkYWlp8653==2'
where (name like '%a' 
  and id not in (select authorid 
                 from news 
                   inner join events on (news.eventid = events.id) 
                 where news.creationdate < events.EVENTDATETIME ));


update coordinators
set role = 
  (select role 
   from (select role, count(*) 
         from coordinators 
         group by eventid, role 
         order by 2) 
  where rownum <=1) 
where eventid in 
  (select eventid 
   from coordinators 
   where userid in 
     (select userid 
      from (select userid, count(userid) 
            from coordinators 
              left join events on (coordinators.eventid = events.id) 
            where cancelled = 0 
            group by userid 
            order by 2 desc)));


update events
set EventDateTime = EventDateTime + 7
where (id not in (select eventid from participants));


delete from coordinators
where eventid in
  (select id 
   from events 
   where creationdate < to_date('03-03-03', 'DD-MM-YY') 
     and cancelled ='1');


update news 
set eventid = null
where eventid in
  (select id 
   from events 
   where creationdate < to_date('03-03-03', 'DD-MM-YY') 
     and cancelled ='1');

-- 1 a oraz 2 (insert)
insert into events 
select max(id)+1, 'added event', null, null, current_date +1, current_date, 3185, 157, 1 from events;

update events
set eventdatetime = eventdatetime +1 where id = 2;


--delete from News where EventId > 19800;
--delete from Coordinators where EventId > 19800;
--delete from Soldtickets where TicketId in
--  (select id from Tickets where EventId > 19800);
--delete from Participants where EventId > 19800;
--delete from Tickets where EventId > 19800;
--delete from events where id > 19800;
  

timing stop script_time
spool off
exit



